<?php  
/**
 * @package QT Loveit
 * 
 */

if(!function_exists('qtli_loveit_link')){
	function qtli_loveit_link($atts){
		extract( shortcode_atts( array(
				'id' => 0,
		), $atts ) );

		ob_start();
		global $post;
		$post_id = $post->ID;
		$vote_count = get_post_meta($post_id, "qtli_votes_count", true);

		if(qtli_hasAlreadyVoted($post_id)) {
			?>
			<a class="qt-loveit-link qt-btn qt-disabled" data-post_id="<?php echo esc_attr($post->ID); ?>" href="#">
		        <span class="qtli like" title="<?php echo esc_attr__("I like this article","qt-loveit") ?>"><i class="dripicons-heart"></i></span>
		        <span class="qtli count"><?php echo esc_attr($vote_count); ?></span>
		    </a>
			<?php echo esc_attr__("You love this post. Total votes: ","qt-loveit") ?><?php echo esc_attr($vote_count); ?>
			<?php  
		} else {
			?>
			    <a class="qt-loveit-link qt-btn qt-btn-secondary" data-post_id="<?php echo esc_attr($post->ID); ?>" href="#">
			        <span class="qtli like" title="<?php echo esc_attr__("I like this article","qt-loveit") ?>"><i class="dripicons-heart"></i></span>
			        <span class="qtli count"><?php echo esc_attr($vote_count); ?></span>
			    </a>
			<?php
		}
		wp_reset_postdata();
		return ob_get_clean();
	}
}
add_shortcode( 'qt-loveit-link', 'qtli_loveit_link' );


if(!function_exists('qtli_loveit_count')){
	function qtli_loveit_count($atts){
		extract( shortcode_atts( array(
				'id' => 0,
		), $atts ) );
		ob_start();
		global $post;
		$post_id = $post->ID;
		$number = get_post_meta($post_id, "qtli_votes_count", true);
		if("" === $number) {
			$number = 0;
		}
		echo  $number;
		return ob_get_clean();
	}
}
add_shortcode( 'qt-loveit-count', 'qtli_loveit_count' );