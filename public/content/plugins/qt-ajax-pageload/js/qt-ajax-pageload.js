/**====================================================================
 *
 *  QT Ajax Page Loader main script
 *  @author QantumThemes
 *  
 ====================================================================**/
(function($) {
	"use strict";
	$("body").append('<div id="qtajaxpreloadericon"><i class="dripicons-loading" ></i></div>');
	var qtAplSelector ="#maincontent",
		qtAplMaincontent = $(qtAplSelector),
		atAplPreloader = $("#qtajaxpreloadericon");
	
	/**
	 * [Before switching content let's scroll to top]
	 * @return {[bol]}
	 */
	$.fn.qtAplScrollTop = function(){
		$('html, body').animate({
			scrollTop: 0
		},100, 'easeOutExpo');
		return true;
	};
	
	/**
	 * [Main ajax initialization function]
	 */
	$.fn.qtAplInitAjaxPageLoad = function(){
		$("body").off("click",'a');

		/**
		 * [Bind click function to all the links]
		 */
		$("body").on("click",'a', function(e) {
			var that = $(this),
				href = $(this).attr('href');

			if(href === undefined){
				return e;
			}
			if(href === ""){
				return e;
			}
			/**
			 * [exceptions that will skip ajax loading]
			 */
			if ( (href.match(/^https?\:/i)) && (!href.match(document.domain))  || that.attr("target") === '_blank' || that.hasClass("noajax") || that.attr("type") === 'submit' || that.attr("type") === 'button' || href.match("\/respond|\/wp-admin|mailto:|\/checkout|.zip|.jpg|.gif|.mp3|.pdf|.png|.rar|\/product|\/shop|\/cart|#noajax|download_file") ) {
				return true;
			} 
			if(href.match(document.domain) ){
				e.preventDefault();
				try {
					if (window.history.pushState) {
						var pageurl = href;
						if (pageurl !== window.location) {
							window.history.pushState({
							path: pageurl,
							state:'new'
							}, '', pageurl);
						}
					}
				} catch (e) {
					console.log (e);
				}
				
				/**
				 * Close the sidebar and player
				 */
				$('.button-collapse').sideNav('hide');
				$('.button-playlistswitch').sideNav('hide');
				$("li.current_page_item").removeClass("current_page_item");
				that.closest("li").addClass("current_page_item");
				atAplPreloader.addClass("qt-visible");

				qtAplMaincontent.fadeTo( "fast" ,0, function() {
					 $.fn.qtAplScrollTop();
				}).promise().done(function(){
					qtAplExecuteAjaxLink(href);
				});
			}
		});

		/**
		 * [ajax call]
		 * @param  {[text]} link [url to load]
		 * @return {[bol]}
		 */
		function qtAplExecuteAjaxLink(link){
			var docClass, parser;
			$.ajax({
				url: link,
				success:function(data) {
					/*
					*   Retrive the contents
					*/
				
					$.ajaxData = data;
					parser = new DOMParser();
					$.qtAplAjaxContents = $($.ajaxData).filter(qtAplSelector).html();
					$.qtAplAjaxTitle = $($.ajaxData).filter("title").text();
					docClass = $($.ajaxData).filter("body").attr("class");

					$.qtAplBodyMatches = data.match(/<body.*class=["']([^"']*)["'].*>/);

					
					if(typeof($.qtAplBodyMatches) !== 'undefined'){
					   	docClass = $.qtAplBodyMatches[1];
					}else{
						window.location.replace(link);
					}


					// New method better working: 
					var modifiedAjaxResult = data.replace(/<body/i,'<div id="re_body"').replace(/<\/body/i,'</div'),
						bodyClassesNew = $(modifiedAjaxResult).filter("#re_body").attr("class");

					if(bodyClassesNew){
						docClass = bodyClassesNew;
					}


					$.wpadminbar = $($.ajaxData).filter("#wpadminbar").html(); 
					$.visual_composer_styles = $($.ajaxData).filter('style[data-type=vc_shortcodes-custom-css]').text();					
					/**
					 * [if we have WPML plugin language selector]
					 */
					if($("#qwLLT")){
						$.langswitcher = $($.ajaxData).find("#qwLLT").html(); 
					}

					/*
					*   Start putting the data in the page
					*/
					if(docClass !== undefined && $.qtAplAjaxContents !== undefined){
						$.fn.closeModal();
						$("body").attr("class",docClass);
						$("title").text($.qtAplAjaxTitle);
						$("#wpadminbar").html($.wpadminbar);
						$("#qwLLT").html($.langswitcher);
						if($("style[data-type=vc_shortcodes-custom-css]").length > 0){
							$("style[data-type=vc_shortcodes-custom-css]").append($.visual_composer_styles);
						} else {
							$("head").append('<style type="text/css"  data-type="vc_shortcodes-custom-css">'+$.visual_composer_styles+'</style>');
						}
						qtAplMaincontent.html( $.qtAplAjaxContents ).delay(100).promise().done(function(){
							var scripts = qtAplMaincontent.find("script");
							if(scripts.length > 0){
								scripts.each(function(){
									var code = $(this).html();
									// code = '('+code+')'; // not really needed
									eval(code);
								});	
							}
							if(true === $.fn.initializeAfterAjax()){
								$.fn.initializeVisualComposerAfterAjax();
								$.fn.initializeOnlyAfterAjax();
								atAplPreloader.removeClass("qt-visible");
								qtAplMaincontent.fadeTo( "fast" ,1);
							}else{
								window.location.replace(link);
							}
						});   

					}else{
						window.location.replace(link);
					}
				},
				error: function () {
					//Go to the link normally
					window.location.replace(link);
				}
			});
			return true;
		}
		/**
		 * Manage browser back and forward arrows
		 */
		$(window).on("popstate", function(e) {
			var href;
			if (e.originalEvent.state !== null) {
				href = location.href;
				if(href !== undefined){
					if (!href.match(document.domain))    {
						window.location.replace(href);
					} else {
						// qtAplExecuteAjaxLink(href);
						qtAplMaincontent.fadeTo( "fast" ,0, function() {
							$.fn.qtAplScrollTop();
						}).promise().done(function(){
							qtAplExecuteAjaxLink(href);
						});
					}
				}
			} else {
				href = location.href;
				if(href !== undefined){
					if (!href.match(document.domain))    {
						window.location.replace(href);
					} else {
						qtAplMaincontent.fadeTo( "fast" ,0, function() {
							$.fn.qtAplScrollTop();
						}).promise().done(function(){
							qtAplExecuteAjaxLink(href);
						});
					}
								
				}
			}
		});
	}; // $.fn.qtAplInitAjaxPageLoad

	/**====================================================================
	 *
	 *	Page Ready Trigger
	 * 	This needs to call only $.fn.qtInitTheme
	 * 
	 ====================================================================*/
	jQuery(document).ready(function() {
		$.fn.qtAplInitAjaxPageLoad();		
	});

})(jQuery);
