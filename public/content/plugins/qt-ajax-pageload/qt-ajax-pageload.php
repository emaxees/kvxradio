<?php  
/*
Plugin Name: QT Ajax Pageload
Plugin URI: http://qantumthemes.com
Description: Adds page load with ajax to keep music playing across pages
Version: 1.6
Author: QantumThemes
Author URI: http://qantumthemes.com
*/


/**
 * 	constants
 * 	=============================================
 */
if(!defined('QT_APL_PLUGIN_ACTIVE')) {
	define('QT_APL_PLUGIN_ACTIVE', true);
}
if(!defined('QT_APL_BASE_DIR')) {
	define('QT_APL_BASE_DIR', dirname(__FILE__));
}
if(!defined('QT_APL_BASE_URL')) {
	define('QT_APL_BASE_URL', plugin_dir_url(__FILE__));
}



/**
 * 	includes
 * 	=============================================
 */
// include(QT_APL_BASE_DIR . '/includes/frontend/qtli-shortcode.php');
// include(QT_APL_BASE_DIR . '/includes/backend/qtli-functions.php');

/**
 * 	Enqueue scripts
 * 	=============================================
 */
if(!function_exists('qtapl_enqueue_stuff')){
function qtapl_enqueue_stuff(){

	if(is_user_logged_in()){
		if(current_user_can('edit_pages' )){
			return;
		}
	}
	wp_enqueue_style('qt_ajax_pageload_style', QT_APL_BASE_URL.'qt-apl-style.css' );
	if(get_theme_mod("qt_enable_debug", 0)){

		wp_enqueue_script('qt_ajax_pageload_script', QT_APL_BASE_URL.'js/qt-ajax-pageload.js', array('jquery', 'qantumthemes_main'), '1.0', true );
	} else {
		wp_enqueue_script('qt_ajax_pageload_script', QT_APL_BASE_URL.'js/min/qt-ajax-pageload-min.js', array('jquery', 'qantumthemes_main'), '1.0', true );
	}
	
}}
add_action( 'wp_enqueue_scripts', 'qtapl_enqueue_stuff' );

