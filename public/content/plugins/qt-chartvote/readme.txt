* Works only with the charts of the OnAir2 theme
* There are no options: install and go
* You can vote only once per song
* Voting is tored as session, so restarting browser will allow to vote again
* Hidden for small chart template
* the vote can be positive or negative
* the value is associated with the physical item of the chart, if you delete the song and put another, the number stays
* you can manually edit the rating of a track by editing the chart