<?php
    /*
    Plugin Name: Custom CORS Header
    description: Enable Custom Headers for wordpress rest API
    Version: 1
    Author: Maximiliano Escobar
    */
    function custom_rest_cors() {
        remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
        add_filter( 'rest_pre_serve_request', function( $value ) {
            header('Access-Control-Allow-Origin: *');
            header( 'Access-Control-Allow-Methods: GET, OPTIONS, HEAD' );
            header( 'Access-Control-Allow-Credentials: true' );
            header( 'Access-Control-Expose-Headers: Link', false );
            return $value;
        } );
    }

    add_action( 'rest_api_init', 'custom_rest_cors', 15 );
?>