<?php
define ('CUSTOM_TYPE_QTBANNER','banner');
function qt_banner_type() {
    $name = __(ucfirst(CUSTOM_TYPE_QTBANNER),'_qt');
	$labelssuperbanner = array(
        'name' => $name.'s',
        'singular_name' => $name,
        'add_new' => 'Add New ',
        'add_new_item' => 'Add New '.$name,
        'edit_item' => 'Edit '.$name,
        'new_item' => 'New '.$name,
        'all_items' => 'All '.$name.'s',
        'view_item' => 'View '.$name,
        'search_items' => 'Search '.$name.'s',
        'not_found' =>  'No '.$name.' found',
        'not_found_in_trash' => 'No '.$name.'s found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => $name.'s'
    );
    $args = array(
        'labels' => $labelssuperbanner,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => false, 
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'page',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => 58,
        'menu_icon' => BM_PLUGIN_DIR.'assets/menu-icon.png',
    	'page-attributes' => false,
    	'show_in_nav_menus' => false,
    	'show_in_admin_bar' => true,
    	'show_in_menu' => true,
        'supports' => array('title','page-attributes','thumbnail'/*,'post-formats'*/)
    ); 
    register_post_type( CUSTOM_TYPE_QTBANNER , $args );
    $fields = array(
                    array(
                        'label' => 'Expiration date',
                        'desc'  => 'The banner will be automatically set to "draft" the day after the expiration',
                        'id'    => BM_PREFIX . 'date',
                        'type'  => 'date'
                    ),
                    array(
                        'label' => 'Url',
                        'id'    => BM_PREFIX . 'url',
                        'type'  => 'text'
                    )
                    ,
                    array(
                        'label' => 'Clicks',
                        'id'    => BM_PREFIX.'clicks_out',
                        'type'  => 'readonly'
                    )                
    );
    if(post_type_exists(CUSTOM_TYPE_QTBANNER)){
        if(function_exists('custom_meta_box_field')){
            $sample_box = new custom_add_meta_box(CUSTOM_TYPE_QTBANNER, ucfirst(CUSTOM_TYPE_QTBANNER).' details', $fields, CUSTOM_TYPE_QTBANNER, true );
        }
    }
}
add_action('init', 'qt_banner_type');  


function qt_banner_type_flush() {
    // First, we "add" the custom post type via the above written function.
    // Note: "add" is written with quotes, as CPTs don't get added to the DB,
    // They are only referenced in the post_type column with a post entry, 
    // when you add a post of this CPT.
    qt_banner_type();

    // ATTENTION: This is *only* done during plugin activation hook in this example!
    // You should *NEVER EVER* do this on every page load!!
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'qt_banner_type_flush' );


/* = Add some columns to the banner post type list
==============================================================*/

add_filter( 'manage_edit-banner_columns', 'my_banner_columns' ) ;
function my_banner_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'ID' => 'ID',
        'title' => __( 'Banner' ,'_s'),
        'expiration_date' => __( 'Expiration date (Y-M-D)','_s'  ),
        'clicks_out' => __( 'Clicks' ,'_s'),
        // 'banner_impressions' => __( 'Impressions' ,'_s'),
         
        'image' => __( 'Image','_s' )
    );
    return $columns;
}


add_action( 'manage_banner_posts_custom_column', 'my_manage_banner_columns', 10, 2 );
function my_manage_banner_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {
        case 'ID' :
            echo $post_id;
            break;
        case 'expiration_date' :
            $expiration_date = get_post_meta( $post_id, BM_PREFIX . 'date', true );
            if ( empty( $expiration_date ) )
                echo __( 'Unknown','_qt' );
            else
                echo $expiration_date;
            break;

         case 'clicks_out' :
            $x = get_post_meta( $post_id, BM_PREFIX . 'clicks_out', true );
            if ( empty( $x ) )
                echo __( 'Unknown' ,'_qt');
            else
                echo $x;
            break;  
        // case 'banner_impressions' :
        //     $x = get_post_meta( $post_id, BM_PREFIX . 'banner_impressions', true );
        //     if ( empty( $x ) )
        //         echo "0";
        //     else
        //         echo $x;
        //     break;  
        case 'image' :
            if(has_post_thumbnail($post_id)){
                the_post_thumbnail(array("50","50"),array( 'class' => "img-responsive" ));
                
            }
            break;    
        default :
            break;
    }
}



add_filter( 'manage_edit-banner_sortable_columns', 'my_banner_sortable_columns' );

function my_banner_sortable_columns( $columns ) {
    $columns['clicks_out'] = 'clicks_out';
    $columns['expiration_date'] = 'expiration_date';
    return $columns;
}
