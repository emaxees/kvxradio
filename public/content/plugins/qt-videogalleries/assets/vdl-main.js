/* = quicksand filter
======================================================*/
jQuery.noConflict();
/**====================================================================
 *
 *
 * 	02. Check images loaded in a container
 *
 * 
 ====================================================================*/
jQuery.fn.imagesLoaded = function () {
	// get all the images (excluding those with no src attribute)
	var $imgs = this.find('img[src!=""]');
	// if there's no images, just return an already resolved promise
	if (!$imgs.length) {return jQuery.Deferred().resolve().promise();}
	// for each image, add a deferred object to the array which resolves when the image is loaded (or if loading fails)
	var dfds = [];  
	$imgs.each(function(){
		var dfd = jQuery.Deferred();
		dfds.push(dfd);
		var img = new Image();
		img.onload = function(){dfd.resolve();};
		img.onerror = function(){dfd.resolve();};
		img.src = this.src;
	});
	// return a master promise object which will resolve when all the deferred objects have resolved
	// IE - when all the images are loaded
	return jQuery.when.apply(jQuery,dfds);
};
jQuery.fn.removeMyStyle = function(style){
	if(typeof(style) !== "undefined" && style !== undefined){
	    var search = new RegExp(style + '[^;]+;?', 'g');
	    return this.each(function()    {
	        jQuery(this).attr('style', function(i, styles){
	        	if(styles !== undefined){
	            return styles.replace(search, '');	        		
	        	}

	        });
	    });
	}
};
// auto background activation =======================================//	
jQuery.vdl_autoBgStyles = function(){
	jQuery("[data-bgimg]").each(function(){
		var img = jQuery(this).attr("data-bgimg");
		if(img !== ""){
			jQuery(this).css({"background": "url("+img+")","background-size":"cover" });
		}
	});	
};
jQuery.vdl_autoBgStyles();

// Quicksand activation =======================================//
jQuery.activateFilterBlocks = function(){
  	var jQueryfilterType = jQuery('.filterOptions li.active a').attr('class');
  	var jQueryholder = jQuery('.filterable-grid');
  	var jQuerydata = jQueryholder.clone();
  	var filteredData;
	jQuery('.filterOptions a').click(function(e) {
		e.preventDefault();
		jQuery('.filterOptions li').removeClass('active');
		jQueryfilterType = jQuery(this).attr('class');
		jQuery(this).parent().addClass('active');
		if (jQueryfilterType === 'all') {
			filteredData = jQuerydata.find('.fgchild');
		} else {
			filteredData = jQuerydata.find('.fgchild[data-type~=' + jQueryfilterType + ']');
		}
		// call quicksand and assign transition parameters
		jQueryholder.quicksand(filteredData, {
			duration: 200,
			adjustHeight: 'dynamic' ,
			easing: 'easeInOutQuad'
		});
		return false;
	});
	jQuery("#vdlsubpagesAll").click();
};
jQuery.vdl_todoAfterResize = function(){
	if(jQuery.beingExecuted === 0){
		jQuery.beingExecuted = 1;
		jQuery(".filterable-grid").css({opacity:0});
		jQuery(".filterable-grid").each(function(){
			var that = jQuery(this);		
			var width = that.find(".vdl-subpages-item:first-child").outerWidth(),
				height = width/16*9;
			that.find('.vdl-element').each(function(){
				jQuery(this).css({height:height+"px",width:width+"px"});
			});
			that.animate({opacity:1},30);
		});
		jQuery.activateFilterBlocks();
		jQuery(".filterable-grid").animate({opacity:1},50);
	}
	jQuery.beingExecuted = 0;
};
jQuery.vdl_FixBlocks = function() {
	jQuery.vdl_todoAfterResize();
};
jQuery.vdl_OnResize = function() {
	jQuery('.vdl-element, .filterable-grid').removeMyStyle("width");
	jQuery.vdl_FixBlocks(1);
};
jQuery.vdl_Init = function(){
	jQuery.beingExecuted = 0;
	jQuery.vdl_autoBgStyles();
	jQuery.vdl_FixBlocks(1);	
};
jQuery(window).load(function() {
	jQuery("body").imagesLoaded().then(function(){
		jQuery.vdl_Init();
	});
});

jQuery(window).on('resize', function() {
	jQuery.vdl_OnResize();
});

