<?php
/*
Package: OnAir2
Description: WIDGET ABOUT
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/


add_action( 'widgets_init', 'qantumthemes_about_widget' );
function qantumthemes_about_widget() {
	register_widget( 'qantumthemes_About_widget' );
}

class qantumthemes_About_widget extends WP_Widget {
	/**
	 * [__construct]
	 * =============================================
	 */
	public function __construct() {
		$widget_ops = array( 'classname' => 'qtaboutwidget', 'description' => esc_attr__('A widget that displays plain info with a link', "qt-widgets") );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'qtaboutwidget-widget' );
		parent::__construct( 'qtaboutwidget-widget', esc_attr__('QT About Info', "qt-widgets"), $widget_ops, $control_ops );
	}
	/**
	 * [widget]
	 * =============================================
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		echo $before_widget;
		if(array_key_exists("title",$instance)){
			echo $before_title.apply_filters("widget_title", $instance['title'], "qtaboutwidget-widget").$after_title; 
		}
		?>
		<div class="qt-widget-about">
			<p>
				<?php  
				if(array_key_exists("content",$instance)){
					echo html_entity_decode (esc_html(  $instance['content'] )); 
				}?>
				<br>
				<?php  
				if(array_key_exists("link",$instance) && array_key_exists("anchor",$instance)){
					if($instance['link'] != ''){
						?><a href="<?php echo esc_url($instance['link']); ?>"><?php echo esc_attr($instance['anchor']); ?> <i class="dripicons-arrow-thin-right"></i></a><?php  
					}			
				}
				?>
			</p>
		</div>
		<?php
		echo $after_widget;
	}

	/**
	 * [update save the parameters]
	 * =============================================
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//Strip tags from title and name to remove HTML 

		$attarray = array(
			'title',
			'content',
			'link',
			'anchor'
		);
		foreach ($attarray as $a){
			$instance[$a] = strip_tags( $new_instance[$a] );
		}
		return $instance;
	}

	/**
	 * [form widget parameters form]
	 * =============================================
	 */
	public function form( $instance ) {
		$defaults = array( 
				'title' => esc_attr__('About', "qt-widgets"),
				'content' => esc_attr__('QantumThemes is a dynamic wordpress company creating cutting edge music wordpress themes.', "qt-widgets"),
				'link' => 'http://qantumthemes.com',
				'anchor' => esc_attr__('Discover more', "qt-widgets")
				);

		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	 	<h2><?php echo esc_attr__("Options", "qt-widgets"); ?></h2>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo esc_attr__('Title:', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'content' ); ?>"><?php echo esc_attr__('Text (no html)', "qt-widgets"); ?></label>
			<textarea rows="4" id="<?php echo $this->get_field_id( 'content' ); ?>" name="<?php echo $this->get_field_name( 'content' ); ?>" style="width:100%;"><?php echo esc_html( $instance['content'] ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php echo esc_attr__('Link URL:', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" value="<?php echo $instance['link']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'anchor' ); ?>"><?php echo esc_attr__('Link anchor text:', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'anchor' ); ?>" name="<?php echo $this->get_field_name( 'anchor' ); ?>" value="<?php echo $instance['anchor']; ?>" style="width:100%;" />
		</p>
	<?php
	}
}

