<?php
/*
Package: OnAir2
Description: WIDGET RECENT POST
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/

add_action( 'widgets_init', 'qantumthemes_postarchive_widget' );
function qantumthemes_postarchive_widget() {
	register_widget( 'qantumthemes_Postarchive_widget' );
}

class qantumthemes_Postarchive_widget extends WP_Widget {
	/**
	 * [__construct]
	 * =============================================
	 */
	public function __construct() {
		$widget_ops = array( 'classname' => 'qtpostarchive', 'description' => esc_attr__('Blog archive with background image', "qt-widgets") );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'qtpostarchive-widget' );
		parent::__construct( 'qtpostarchive-widget', esc_attr__('QT Posts Archive', "qt-widgets"), $widget_ops, $control_ops );
	}
	/**
	 * [widget]
	 * =============================================
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		echo $before_widget;
		if(array_key_exists("title",$instance)){
			echo $before_title.apply_filters("widget_title", $instance['title'], "qtpostarchive-widget").$after_title; 
		}

		if(!array_key_exists('number', $instance)) {
			$instance['number'] = 3;
		}
		if(!is_numeric($instance['number'])){
			$instance['number'] = 3;
		}

		/**
         * [$args Query arguments]
         * @var array
         */
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => $instance['number'],
            'post_status' => 'publish',
            'ignore_sticky_posts' => 1,
            'suppress_filters' => false,
            'paged' => 1
        );

        /**
         * [$wp_query execution of the query]
         * @var WP_Query
         */
        $wp_query = new WP_Query( $args );
        if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
            $post = $wp_query->post;
            setup_postdata( $post );
            global $post;
           	?>

			<div class="qt-part-archive-item qt-part-archive-item-postarchive">
				<div class="qt-item-header">
					<div class="qt-header-top">
						<ul class="qt-tags">
							<li><?php the_category('</li><li>'); ?></li>
						</ul>
						
				    </div>
				    <div class="qt-header-mid qt-vc">
						<div class="qt-vi">
					  		<h4 class="qt-title">
								<a href="<?php the_permalink(); ?>" class="qt-text-shadow">
									<?php the_title(); ?>
								</a>
							</h4>
						</div>
					</div>
					<div class="qt-header-bottom">
						<a href="<?php the_permalink(); ?>" class="qt-btn qt-btn-primary qt-readmore"><i class="dripicons-align-justify"></i></a>
					</div>
					<?php 
					/**
					 *
					 *	Featured image background
					 * 
					 */
					if (has_post_thumbnail()){ ?>
				        <div class="qt-header-bg" data-bgimage="<?php echo the_post_thumbnail_url( 'qantumthemes-medium' ); ?>">
				            <?php the_post_thumbnail( 'qantumthemes-medium',array('class'=>'img-responsive') ); ?>
				        </div>
			     	<?php } ?>
				</div>
			</div>

           	<?php  
            
        endwhile; else: ?>
            <h5><?php echo esc_attr__("No posts yet.","qt-widgets")?></h5>
        <?php endif;
        wp_reset_postdata();
		echo $after_widget;
	}

	/**
	 * [update save the parameters]
	 * =============================================
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//Strip tags from title and name to remove HTML 

		$attarray = array(
			'title',
			'number'=> '3'
		);
		foreach ($attarray as $a){
			$instance[$a] = strip_tags( $new_instance[$a] );
		}
		return $instance;
	}

	/**
	 * [form widget parameters form]
	 * =============================================
	 */
	public function form( $instance ) {
		$defaults = array( 
				'title' => esc_attr__('News', "qt-widgets"),
				'number' => '3'
				);

		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	 	<h2><?php echo esc_attr__("Options", "qt-widgets"); ?></h2>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo esc_attr__('Title:', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo esc_attr__('Number of posts:', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" style="width:100%;" />
		</p>
	<?php
	}
}
