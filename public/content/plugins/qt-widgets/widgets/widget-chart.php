<?php
/*
Package: OnAir2
Description: WIDGET CHART
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/


add_action( 'widgets_init', 'qantumthemes_chart_widget' );
function qantumthemes_chart_widget() {
	register_widget( 'qantumthemes_chart_widget' );
}

class qantumthemes_chart_widget extends WP_Widget {
	/**
	 * [__construct]
	 * =============================================
	 */
	public function __construct() {
		$widget_ops = array( 'classname' => 'qtchartwidget', 'description' => esc_attr__('Display tracks from a music chart', "qt-widgets") );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'qtchartwidget-widget' );
		parent::__construct( 'qtchartwidget-widget', esc_attr__('QT Music Charts', "qt-widgets"), $widget_ops, $control_ops );
	}
	/**
	 * [widget]
	 * =============================================
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		extract( $instance );
		echo $before_widget;
		if($title){
			echo $before_title.apply_filters("widget_title", $title, "qtchartwidget-widget").$after_title; 
		}
		?>
		<div class="qt-widget-chart">
			<?php echo do_shortcode('[qt-chart number="'.$number.'" chartstyle="chart-small" chartcategory="'.$chartcategory.'" showtitle="'.$showtitle.'" showthumbnail="'.$showthumbnail.'"]' ); ?>
		</div>
		<?php
		echo $after_widget;
	}

	/**
	 * [update save the parameters]
	 * =============================================
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//Strip tags from title and name to remove HTML 
		$attarray = array(
			'title',
			'chartid',
			'chartcategory',
			'number',
			'showtitle',
			'showthumbnail'
		);
		foreach ($attarray as $a){
			$instance[$a] = strip_tags( $new_instance[$a] );
		}
		return $instance;
	}

	/**
	 * [form widget parameters form]
	 * =============================================
	 */
	public function form( $instance ) {
		$defaults = array( 
				'title' => "Chart",
				'chartid' => false,
				'number' => 5,
				'chartcategory' => '',
				'showtitle' => 0,
				'showthumbnail' => 0
				);

		$instance = wp_parse_args( (array) $instance, $defaults ); 
		?>
	 	<h2><?php echo esc_attr__("Options", "qt-widgets"); ?></h2>
	 	<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo esc_attr__('Title', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'chartid' ); ?>"><?php echo esc_attr__('Chart ID', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'chartid' ); ?>" name="<?php echo $this->get_field_name( 'chartid' ); ?>" value="<?php echo $instance['chartid']; ?>" style="width:100%;" />
			<small><?php echo esc_attr("If no ID is specified, the latest chart will be used") ?></small>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'chartcategory' ); ?>"><?php echo esc_attr__('Category filter', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'chartcategory' ); ?>" name="<?php echo $this->get_field_name( 'chartcategory' ); ?>" value="<?php echo $instance['chartcategory']; ?>" style="width:100%;" />
			<small><?php echo esc_attr("Specify a chart category SLUG. If the ID is empty, the widget will automatically extract the latest chart from this chart category.") ?></small>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo esc_attr__('Track amount', "qt-widgets"); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'showtitle' ); ?>"><?php echo esc_attr__('Display the chart title', "qt-widgets"); ?></label><br />			
			<?php echo esc_attr__("Display","qt-widgets"); ?>  <input type="radio" name="<?php echo $this->get_field_name( 'showtitle' ); ?>" value="1" <?php if($instance['showtitle'] == '1'){ echo ' checked= "checked" '; } ?> />  
			<?php echo esc_attr__("Hide","qt-widgets"); ?>  <input type="radio" name="<?php echo $this->get_field_name( 'showtitle' ); ?>" value="0" <?php if($instance['showtitle'] !== '1'){ echo ' checked= "checked" '; } ?> />  
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'showthumbnail' ); ?>"><?php echo esc_attr__('Display the thumbnail', "qt-widgets"); ?></label><br />			
			<?php echo esc_attr__("Display","qt-widgets"); ?>  <input type="radio" name="<?php echo $this->get_field_name( 'showthumbnail' ); ?>" value="1" <?php if($instance['showthumbnail'] == '1'){ echo ' checked= "checked" '; } ?> />  
			<?php echo esc_attr__("Hide","qt-widgets"); ?>  <input type="radio" name="<?php echo $this->get_field_name( 'showthumbnail' ); ?>" value="0" <?php if($instance['showthumbnail'] !== '1'){ echo ' checked= "checked" '; } ?> />  
		</p>
	<?php
	}
}

