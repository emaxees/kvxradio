<?php
/**
 * @package t2gconnectorclient
 * @author Themes2Go
 */

/**
 * Plugin installer is created by the TGM Connector library
 */
function t2gconnectorclient_action_form(){
	if(false == t2gconnectorclient_check_code() && true == t2gconnector_deepcheck()){
		return;
	}
	?>
	<p>How to install the required plugins</p>
	<img src="<?php echo plugins_url( 't2gconnectorclient/assets/img/instructions-plugins.png' ); ?>" class="t2gconnectorclient-instruction-image
	">
	<p>If you don't see any plugin in the list, click "Plugins" on the <strong>top of this page</strong>. Still nothing? You're up to date.</p>
	<h3>Plugin installation errors</h3>
	<p>If you are experiencing any error during the installation, <strong>please update your theme</strong>, even if you just downloaded it few minutes ago, as <strong>updates can happen anytime</strong>.</p>
	<?php
	return;
}