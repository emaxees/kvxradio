<?php 

/**
 * URL slug of the dashboard page
 */
function t2gconnectorclient_dashboard_path(){
	return 't2gconnectorclient-dashboard';
}

/**
 * Redirect after activation
 */
add_action('admin_init', 't2gconnectorclient_plugin_redirect');
function t2gconnectorclient_plugin_activate() {
    add_option('t2gconnectorclient_plugin_do_activation_redirect', true);
}
function t2gconnectorclient_plugin_redirect() {
    if (get_option('t2gconnectorclient_plugin_do_activation_redirect', false)) {
        delete_option('t2gconnectorclient_plugin_do_activation_redirect');

        $t2gconnector_deepcheck = t2gconnector_deepcheck();
        if(t2gconnector_deepcheck()){
        	$tab = 'admin-settings';
        } else {
        	$tab = 'admin-plugins';
        }
        

        wp_redirect(admin_url("admin.php?page=".t2gconnectorclient_dashboard_path().'&tab='.$tab ));
    }
}


/**
 * This is for the theme to allow class importing
 */
class T2gConnectorClient {
	public function __construct(){
		return true;
	}
}

/**
 * Return registration URL
 * @return [type] [description]
 */
function t2gconnectorclient_registration_page(){
	$theme_data = wp_get_theme();
	if($theme_data->parent()):
		$theme_name =  $theme_data->parent()->get( 'Name' );
	else:
		$theme_name = $theme_data->get('Name');
	endif;
	$url = t2gconnectorclient_service_url().'connector/';
	$vars = array(
		"t2gconnector_return_url" => urlencode(admin_url( "admin.php?page=t2gconnectorclient-dashboard&tab=admin-settings" )),
		"t2gconnector_action" => "generate_prodkey",
		"t2gconnector_sku" =>	esc_attr(t2gconnector_product_sku()),
		"t2gconnector_purchase_code" => urlencode( base64_encode(t2gconnectorclient_get_purchase_code())),
		"t2gconnector_name" => esc_attr(urlencode($theme_name)),
		"url" => urlencode(get_home_url())
	);
	return add_query_arg($vars, $url);
}

/**
 * Return product key page
 * @return [type] [description]
 */
function t2gconnectorclient_product_key_page(){
	$theme_data = wp_get_theme();
	if($theme_data->parent()):
		$theme_name =  $theme_data->parent()->get( 'Name' );
	else:
		$theme_name = $theme_data->get('Name');
	endif;
	$url = t2gconnectorclient_service_url().'product-page/';
	$vars = array(
		"t2gconnector_return_url" => urlencode($_SERVER["REQUEST_URI"]),
		"t2gconnector_action" => "generate_prodkey",
		"t2gconnector_sku" =>	esc_attr(t2gconnector_product_sku()),
		"t2gconnector_purchase_code" => urlencode( base64_encode(t2gconnectorclient_get_purchase_code())),
		"t2gconnector_name" => esc_attr(urlencode($theme_name)),
		"url" => urlencode(get_home_url())

		);
	return add_query_arg($vars, $url);
}



/*
*	We can override the config from the theme
*/
function t2gconnector_product_sku(){
	if(class_exists('T2GConnectorConfig')){
		$newConfig = new T2GConnectorConfig();
		return $newConfig->t2gconnector_product_sku();
	}
	return false;
}


/*
*	We can override the config from the theme
*/
function t2gconnector_service_url(){
	// return 'pippo';
	if(class_exists('T2GConnectorConfig')){
		
		$newConfig = new T2GConnectorConfig();
		$settings = $newConfig->t2gconnector_settings();
		return $settings['service_url'];
	}
	return false;
}






/**
 * Provides server URL
 * @return [string] [url]
 */
function t2gconnectorclient_service_url(){
	return t2gconnector_service_url();
}


function t2gconnector_product_management_page(){
	if(class_exists('T2GConnectorConfig')){
		$newConfig = new T2GConnectorConfig();
		$settings = $newConfig->t2gconnector_settings();
		return $settings['product_management_page'];
	}
	return false;
}


/*
*	We can override the config from the theme
*/
function t2gconnector_plugins_list(){
	if(class_exists('T2GConnectorConfig')){
		$newConfig = new T2GConnectorConfig();
		return $newConfig->t2gconnector_plugins_list();
	}
	return false;
}


function t2gconnector_demos_list(){
	if(class_exists('T2GConnectorConfig')){
		$newConfig = new T2GConnectorConfig();
		return $newConfig->t2gconnector_demos_list();
	}
	return false;
}


/**
 *
 *	The plugin textdomain
 * 
 */
if(!function_exists('t2gconnectorclient_load_plugin_textdomain')){
function t2gconnectorclient_load_plugin_textdomain() {
  load_plugin_textdomain( 't2gconnectorclient', FALSE, basename( dirname( __FILE__ ) ) . '/languages' );
}}
add_action( 'plugins_loaded', 't2gconnectorclient_load_plugin_textdomain' );




/**
 * Theme details
 */
function t2connectorclient_theme_slug(){
	$theme = get_template();
	return $theme;
}

function t2connectorclient_get_local_theme_version() {
	$theme_data = wp_get_theme();
	if($theme_data->parent()):
		return $theme_data->parent()->get('Version');
	else:
		return $theme_data->get('Version');
	endif;
}





/**
 * Get product key
 */
 function t2gconnectorclient_get_product_key() {
	$prod_k = get_option("t2gconnectorclient_product_activation_key", "");
	if(!$prod_k){
		return false;
	}
	return $prod_k;
}


/**
 * Verify is the product key is correct
 * @return [boolean]
 */
function t2gconnectorclient_check_code($prod_k = false){
	if(false == t2gconnector_deepcheck()){
		return true;
	}

	if(false == $prod_k){
		$prod_k = t2gconnectorclient_get_product_key();
	}
	
	if($prod_k !== ''){
		$data_array = t2gconnectorclient_decode_product_key($prod_k); 
		if(count($data_array) == 3){
			if( esc_url( get_home_url()) == esc_url($data_array['domains'])){
				return true;
				?><p><?php echo esc_attr__("Your product ID:", "t2gconnectorclient").' '. esc_attr($data_array['product_id']);?></p><?php 
			}
		}
	}
	return false;
}


/**
 * returns array with purchase code and domains
 */
if(!function_exists("t2gconnectorclient_decode_product_key")){
	function t2gconnectorclient_decode_product_key($key){
		$decoded = explode('|dn-|', base64_decode($key));
		if(isset( $decoded[2])){
			return array("purchase_code" => $decoded[0], "domains" => $decoded[1], "product_id" => $decoded[2]);
		} else {
			return false;
		}
	}
}
/**
 * Calls the License Manager API to get the license information for the
 * current product.
 *
 * @return object|bool   The product data, or false if API call fails.
 */
 function t2gconnectorclient_get_purchase_code() {
	$prod_k = t2gconnectorclient_get_product_key();
	if(!$prod_k){
		return false;
	}
	$license_info = t2gconnectorclient_decode_product_key($prod_k);
	if(!array_key_exists('purchase_code',  $license_info)){
		return false;
	}
	$p_code = $license_info["purchase_code"];
	return $license_info["purchase_code"];
}


/**
 * returns array with purchase code and domains
 */
// if(!function_exists("t2gconnectorclient_get_purchase_code")){
// 	function t2gconnectorclient_get_purchase_code(){
// 		$key = get_option("t2gconnectorclient_product_activation_key", "");
// 		$decoded = explode('|dn-|', base64_decode($key));
// 		if(isset( $decoded[2])){
// 			return  $decoded[0];
// 		} else {
// 			return false;
// 		}
// 	}
// }



/**
* Returns current plugin version.
* @return string Plugin version. Needs to stay here because of plugin file path
*/
function t2gconnectorclient_plugin_get_version() {
	if ( is_admin() ) {
		$plugin_data = get_plugin_data( __FILE__ );
		$plugin_version = $plugin_data['Version'];
	} else {
		$plugin_version = get_file_data( __FILE__ , array('Version'), 'plugin');
	}
	return $plugin_version;
}


add_action("admin_enqueue_scripts",'t2gconnectorclient_admin_enqueue_scripts');
if(!function_exists('t2gconnectorclient_admin_enqueue_scripts')){
function t2gconnectorclient_admin_enqueue_scripts($hook)
{
	/**
	 * Styles registration
	 */
	wp_register_style('t2gconnectorclient_backend_style',plugins_url( '../assets/style.css' , __FILE__ ), false, $version = false);
	wp_register_script('t2gconnectorclient_script',plugins_url( '../assets/t2gconnectorclient-script.js' , __FILE__ ), array("jquery"), $version = false);
	
	
	/**
	 * Styles enqueue
	 */
	wp_enqueue_style( 't2gconnectorclient_backend_style');
	wp_enqueue_script( 't2gconnectorclient_script' );

}}


/*
*	Allow functions without deep activation check
*/
function t2gconnector_deepcheck(){
	// return 'pippo';
	if(class_exists('T2GConnectorConfig')){
		
		$newConfig = new T2GConnectorConfig();
		$settings = $newConfig->t2gconnector_settings();
		return $settings['deepcheck'];
	}
	return true; //by default we enable deep check
}


/*
*	Allow functions without deep activation check
*/
function t2gconnector_is_autoupdate(){
	// return 'pippo';
	if(class_exists('T2GConnectorConfig')){
		
		$newConfig = new T2GConnectorConfig();
		$settings = $newConfig->t2gconnector_settings();
		if(array_key_exists('auto_update_theme', $settings)){
			return $settings['auto_update_theme'];
		}
	}
	return false; //by default we enable deep check
}