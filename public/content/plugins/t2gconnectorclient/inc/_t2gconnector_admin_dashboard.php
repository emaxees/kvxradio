<?php
/**
 * @package t2gconnectorclient
 * @author Themes2Go
 */

include ('tabs/tab-settings-form.php');
include ('tabs/tab-action-form.php');
include ('tabs/tab-theme-update.php');
include ('tabs/tab-demo-import.php');

/**
 * Register a custom menu page.
 */
function t2gconnectorclient_register_my_custom_menu_page(){
	$menu_title = 'Theme Dashboard';
	$icon = 'dashicons-admin-home';

	/* Dynamic contents from the THEME in t2gconnectorclient-config */
	if(class_exists('T2GConnectorConfig')){
		$newConfig = new T2GConnectorConfig();
		$settings = $newConfig->t2gconnector_settings();
		if(array_key_exists('dashboard_title', $settings)){
			$menu_title = $settings['dashboard_title'];
		}
		if(array_key_exists('icon', $settings)){
			$icon = $settings['icon'];
		}
	}
	add_menu_page( 
		$menu_title,
		$menu_title,
		'manage_options',
		t2gconnectorclient_dashboard_path(),
		't2gconnectorclient_custom_menu_page',
		$icon,
		0
	); 
}
add_action( 'admin_menu', 't2gconnectorclient_register_my_custom_menu_page' );



if(!function_exists('t2gconnectorclient_theme_version')){
	function t2gconnectorclient_theme_version(){
		if (wp_get_theme()->parent() !== false ){
			$version  = wp_get_theme()->parent()->get( 'Version' );
		} else {
			$version  = wp_get_theme()->get( 'Version' );
		}
		return $version;
	}
}

/* = Main function that creates the options page
==========================================================================*/
function t2gconnectorclient_custom_menu_page(){


	/**
	 * Check if the theme is correct (needs to have settings in theme config file)
	 */
	if(false === t2gconnector_product_sku()){
		?>
		<h5>
			<?php echo esc_attr__("Sorry, this plugin cannot work with your theme (Product ID missing in theme configuration)", "t2gconnectorclient"); ?>
		</h5>
		<?php
		return;
	}


	/* Dynamic contents from the THEME in t2gconnectorclient-config */
	?>
	<div class="t2gconnectorclient-dashboard-container">
		<?php




		/**
		 * Logo
		 */
		if(class_exists('T2GConnectorConfig')){
			$newConfig = new T2GConnectorConfig();
			$settings = $newConfig->t2gconnector_settings();

			/**
			 * Verify if theme is up to date
			 */
			if(is_array($settings)) {
				if(array_key_exists('messageboard_top', $settings)){

					$theme_version =  t2gconnectorclient_theme_version();

					if( $theme_version ){
						$messageboard_top_url = add_query_arg('theme_version', t2gconnectorclient_theme_version(),  $settings['messageboard_top']);
						// echo $messageboard_top_url;
						$content = wp_remote_get( $messageboard_top_url,  array( 'timeout' => 120) );
						if ( !is_wp_error( $content ) ) {
							?>
							<div class="t2gconnectorclient-messageboard_top">
							<?php echo $content['body']; ?>
							</div>
							<?php
						}
					}
				}
			}
			$logo = '';
			if(array_key_exists('logo', $settings)){
				?><img src="<?php echo esc_url($settings['logo']); ?>" class="t2gconnectorclient-logo"><?php
			}
		}


		/**
		 * Set current tab
		 * @var string
		 */
		$current = 'admin-plugins';
		if(isset($_GET['tab'])){
			$current = $_GET['tab'];
		}


		if(false == t2gconnectorclient_check_code()){
			$current = 'admin-settings';
		}

		if($current == '' || !isset($current)){
			$current = 'admin-plugins';
		}

		?>
			<h2 class="t2gconnectorclient-tab-wrapper t2gconnectorclient-paper t2gconnectorclient-card">
				<?php  


				/**
				 * [$t2gconnector_deepcheck If false we don't check product activation]
				 * @var [type]
				 */
				$t2gconnector_deepcheck = t2gconnector_deepcheck();

				
				/**
				 * [$tabs List of pages of the admin screen]
				 * @var array
				 */
				$tabs = array();
				if($t2gconnector_deepcheck ){
					$tabs['admin-settings'] = 'Product Activation';
				} else {
					$tabs['admin-settings'] = 'Product status';
				}
				$tabs['admin-plugins'] = 'Plugins';
				$tabs['editor-paste-text'] = 'Install demo';


				if(t2gconnector_is_autoupdate()){
					$tabs['welcome-view-site'] = 'Updates and Support';
				} else {
					$tabs['welcome-view-site'] = 'Documentation and support';
				}



				foreach( $tabs as $tab => $name ){
					$class = ( $tab == $current ) ? ' nav-tab-active' : '';
					$path = t2gconnectorclient_dashboard_path();
					?>
						<a class="t2gconnectorclient-nav-tab <?php echo esc_attr($class); ?>" href="<?php echo admin_url("admin.php?page=". esc_attr($path)."&tab=". esc_attr($tab)); ?>">
							<i class="dashicons dashicons-<?php echo esc_attr($tab); ?>"></i>
							<?php echo esc_attr($name);?>
						</a>
					<?php
				}
				?>
			</h2>
			<div class="t2gconnectorclient-dashboard-content t2gconnectorclient-paper t2gconnectorclient-card">
				<?php
				/**
				 * For safety we do not include the spare file alone. Functions must match the external ones loaded at beginning of this file
				 */
				
				?><h1><?php echo $tabs[$current]; ?></h1><?php
				
				switch ($current){
					case "admin-settings" :
						if(function_exists('t2gconnectorclient_settings_form')){
					   		t2gconnectorclient_settings_form();
						}
					break;
					case "editor-paste-text" :
						if(function_exists('t2gconnectorclient_demo_import')){
					   		t2gconnectorclient_demo_import();
						}
					break;
					case "welcome-view-site" :
						if(function_exists('t2gconnectorclient_theme_update_output')){
					   		t2gconnectorclient_theme_update_output();
					   		t2gconnectorclient_support_output();
					   	}
					break;
					case "admin-plugins" :
					default:
						if(function_exists('t2gconnectorclient_action_form')){
							t2gconnectorclient_action_form();	
						}
				}
				?>
		</div>
	</div>
	<?php
}


