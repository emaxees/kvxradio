<?php  
// TEMP: Enable update check on every request. Normally you don't need this! This is for testing only!
if(isset($_GET)){
	if(array_key_exists("page", $_GET) && array_key_exists("tab", $_GET)) {
		if($_GET["page"] == t2gconnectorclient_dashboard_path() && $_GET["tab"] == 'welcome-view-site'){
			set_site_transient('update_themes', null);
		}
	}
}



/**
 * Theme update action
 */
add_filter('pre_set_site_transient_update_themes', 't2g_check_for_update');
add_filter( 'pre_set_transient_update_themes', 't2g_check_for_update' );
function t2g_check_for_update($checked_data) {
	if(false == t2gconnector_product_sku()  || false == t2gconnector_deepcheck()){
		return;
	}
	$prod_k = t2gconnectorclient_get_product_key();
	$curr_ver = t2connectorclient_get_local_theme_version();
	$theme = t2connectorclient_theme_slug();
	$args = array(
		'method' => 'POST',
		'timeout' => 30,
		'body' => array( 't2gconnector_product_key' => $prod_k,  't2gconnector_detected_domain' => get_site_url() )
	);
	$url = t2gconnectorclient_service_url();
	$request = wp_remote_post( $url, $args);
	if ( is_wp_error( $request ) ) {
		return $checked_data;
	}	
	if ( $request['response']['code'] == 200 ) {
		// die($request['body'] );
		$data = unserialize( $request['body'] );
		if (isset($data['error'])) {
			update_option("t2gconnectorclient_".$theme."_license_expired", 1);
			update_option("t2gconnectorclient_".$theme."_remote_ver",   "");
			update_option("t2gconnectorclient_".$theme."_description",   "");
		}
		if(version_compare($curr_ver, $data[$theme]['new_version'], '<')) {
			$checked_data->response[$theme] = $data[$theme];
			update_option("t2gconnectorclient_".$theme."_remote_ver",   $data[$theme]['new_version']);
			update_option("t2gconnectorclient_".$theme."_description",   $data[$theme]['description']);
		}
	}
	return $checked_data;
}



add_action( 'admin_init', 't2gconnector_theme_updater');
function t2gconnector_theme_updater(){

	if(false == t2gconnector_product_sku() || false == t2gconnector_deepcheck()){
		return;
	}
	$prod_k = t2gconnectorclient_get_product_key();
	if(false === $prod_k){
		add_action( 'admin_notices', 't2gconnector_theme_updater_forbidden' );
		return;
	}
	$screen = get_current_screen();
	if(  !isset($_GET['page']) || !isset($_GET['tab'])){
		return;
	}
	if("welcome-view-site" !== $_GET['tab'] || t2gconnectorclient_dashboard_path() !==  $_GET['page']){
		return;
	}
	$remote_ver = get_option("t2gconnectorclient_".t2connectorclient_theme_slug()."_remote_ver", t2connectorclient_get_local_theme_version());
	$curr_ver = t2connectorclient_get_local_theme_version();
	if(version_compare($curr_ver, $remote_ver, '<')){
		add_action( 'admin_notices', 't2gconnector_theme_updater_available' );
	}
}

function t2gconnector_theme_updater_forbidden(){
	$class = 'notice notice-error';
	$message = __( "Add your \"Product Key\" to enable automatic theme and plugins updates and demo contents", 't2gconnectorclient' );
	$settings_url = admin_url( "admin.php?page=".t2gconnectorclient_dashboard_path() ); 
	$link = '<a href="'.$settings_url.'">'.esc_attr__("Click Here", "t2gconnectorclient").'</a>';
	printf( '<div class="wrap"><div class="%1$s"><p>%2$s</p></div></div>', esc_attr( $class ), esc_html( $message ).' '.$link ); 
}

function t2gconnector_theme_updater_available(){
	$class = 'notice notice-success';
	$message = __( "Good news! A new theme version is available!", 't2gconnectorclient' );
	printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}