<?php

/**
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *	NOT IN USE, ALL IN THEME_UPDATER.PHP
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 */











// TEMP: Enable update check on every request. Normally you don't need this! This is for testing only!
// set_site_transient('update_themes', null);

add_filter('pre_set_site_transient_update_themes', 't2g_check_for_update');
add_filter( 'pre_set_transient_update_themes', 't2g_check_for_update' );
function t2g_check_for_update($checked_data) {
	$prod_k = t2gconnectorclient_get_product_key();
	$curr_ver = t2connectorclient_get_local_theme_version();
	$theme = t2connectorclient_theme_slug();
	$args = array(
		'method' => 'POST',
		'timeout' => 30,
		'body' => array( 't2gconnector_product_key' => $prod_k,  't2gconnector_detected_domain' => get_site_url() )
	);
	$url = t2gconnectorclient_service_url();
	$request = wp_remote_post( $url, $args);
	if ( is_wp_error( $request ) ) {
		return $checked_data;
	}	
	if ( $request['response']['code'] == 200 ) {
		// die($request['body'] );
		$data = unserialize( $request['body'] );
		if (isset($data['error'])) {
			update_option("t2gconnectorclient_".$theme."_license_expired", 1);
		}
		if(version_compare($curr_ver, $data[$theme]['new_version'], '<')) {
			$checked_data->response[$theme] = $data[$theme];
			update_option("t2gconnectorclient_".$theme."_remote_ver",   $data[$theme]['new_version']);
		}
	}
	return $checked_data;
}




/**
 * This is not used
 */
if( ! class_exists( 'T2gConnectorClientUpdater' ) ) {
	class T2gConnectorClientUpdater {
		var $prod_k = false;
		function __construct( $product_key ) {
			$this->prod_k = $product_key;

			add_filter( 'pre_set_site_transient_update_themes', array( &$this, 'check_for_update' ) );
			add_filter( 'pre_set_transient_update_themes', array( &$this, 'check_for_update' ) );
			// set_site_transient('update_themes', null);
		}

		public function check_for_update( $checked_data ) {

			$prod_k = t2gconnectorclient_get_product_key();
			$curr_ver = t2connectorclient_get_local_theme_version();
			$theme = t2connectorclient_theme_slug();
			$args = array(
				'method' => 'POST',
				'timeout' => 30,
				'body' => array( 't2gconnector_product_key' => $prod_k,  't2gconnector_detected_domain' => get_site_url() )
			);
			$url = t2gconnectorclient_service_url();
			$request = wp_remote_post( $url, $args);
			if ( is_wp_error( $request ) ) {
				return $checked_data;
			}	
			if ( $request['response']['code'] == 200 ) {
				// die($request['body'] );
				$data = unserialize( $request['body'] );
				if (isset($data['error'])) {
					update_option("t2gconnectorclient_".$theme."_license_expired", 1);
				}
				if(version_compare($curr_ver, $data[$theme]['new_version'], '<')) {
					$checked_data->response[$theme] = $data[$theme];
					update_option("t2gconnectorclient_".$theme."_remote_ver",   $data[$theme]['new_version']);
				}
			}
			return $checked_data;
		}

	}

}